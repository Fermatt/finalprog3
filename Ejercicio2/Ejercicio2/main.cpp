#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Strategy.h"
#include "vld.h"

int main()
{
	sf::RenderWindow window(sf::VideoMode(640, 480), "SFML works!");
	Player* p = new Player(new StratUp());

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		p->update();
		window.clear();
		window.draw(p->getSprite());
		window.display();
	}
	delete p;
	return 0;
}