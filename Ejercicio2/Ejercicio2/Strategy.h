#pragma once
class Strategy
{
public:
	Strategy();
	virtual ~Strategy();
	virtual int MovementX() = 0;
	virtual int MovementY() = 0;
};

class StratUp : public Strategy
{
private:
	const int velX = 0;
	const int velY = 1;
public:
	StratUp();
	virtual ~StratUp();
	virtual int MovementX();
	virtual int MovementY();
};

class StratRight : public Strategy
{
private:
	const int velX = 1;
	const int velY = 0;
public:
	StratRight();
	virtual ~StratRight();
	virtual int MovementX();
	virtual int MovementY();
};

class StratDiag : public Strategy
{
private:
	const int velX = 1;
	const int velY = 1;
public:
	StratDiag();
	virtual ~StratDiag();
	virtual int MovementX();
	virtual int MovementY();
};