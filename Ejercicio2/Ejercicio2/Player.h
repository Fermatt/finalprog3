#include "Strategy.h"
#include "SFML\Graphics.hpp"
#include <iostream>

class Player
{
private:
	Strategy* s;
	sf::Sprite player;
	sf::Texture texture;
public:
	Player(Strategy* _s);
	~Player();
	void update();
	sf::Sprite getSprite();
	void setStrategy(Strategy* _s);
};

