#include "Player.h"



Player::Player(Strategy* _s) : s(_s)
{
	if (!texture.loadFromFile("aestroid_brown.png"))
	{
		std::cout << "Error, cant load image" << std::endl;
	}
	texture.setSmooth(true);
	player.setTexture(texture);
}


Player::~Player()
{
	delete s;
}

void Player::update()
{
	player.move(s->MovementX(), s->MovementY());
}

sf::Sprite Player::getSprite()
{
	return player;
}

void Player::setStrategy(Strategy * _s)
{
	if (s == NULL)
		delete s;
	s = _s;
}
